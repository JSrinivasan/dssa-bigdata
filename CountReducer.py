#!/usr/bin/env python

import sys

currentKey = None
currentCount = 0
key = None

# Iterate over lines from stdin
for line in sys.stdin:

    # Remove leading and trailing whitespace
    line = line.strip()

    # Split into key and count
    key, count = line.split('\t', 1)

    # Convert count to int
    try:
        count = int(count)
    except ValueError:
        # Count was not a number, so silently ignore/discard this line
        continue

    # this IF-switch only works because Hadoop sorts map output
    # by key (here: word) before it is passed to the reducer
    if currentKey == key:
        currentCount += count
    else:
        if currentKey:
            # Write result to stdout
            print '%s\t%s' % (currentKey, currentCount)

        currentCount = count
        currentKey = key

# Output last key
if currentKey == key:
    print '%s\t%s' % (currentKey, currentCount)