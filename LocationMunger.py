#!/usr/bin/env python

import sys
import random
import geopy
import geopy.distance

# List of MMSIs of ships which are bad
bad_ships = (366268080, 367003590, 366996000, 366748250)

# Corresponding headings to displace those ships along
bad_ship_headings = (270, 90, 0, 45)

# Static distance to add on to bad ships (km)
bad_ship_distance = 10.0

# Maximum distance to add on to a good ship (km)
max_good_error = 5.0

# Iterate over each line provided
for line in sys.stdin:
#with open('c:\\data\\ais\\broadcast.csv', 'r') as file:
#    line = file.readline()
#    while line:
    if not line.startswith("X"):    # ignore header
        line = line.strip()             # remove any leading and trailing whitespace
        fields = line.split(",")        # tokenize

        lon = float(fields[0])          # extract lon (X)
        lat = float(fields[1])          # extract lat (Y)
        mmsi = int(fields[10])          # extract MMSI

        if mmsi in bad_ships:
            # Bad ships are always displaced a fixed distance and heading
            distance = bad_ship_distance
            heading = bad_ship_headings[bad_ships.index(mmsi)]
        else:
            # Good ships are always displaced a random distance and heading
            distance = random.uniform(0.0, max_good_error)
            heading = random.uniform(0.0, 359.9)

        # Use geopy to calculate simulated emitter location
        start = geopy.Point(lat, lon)
        d = geopy.distance.VincentyDistance(kilometers=distance)
        end = d.destination(point=start, bearing=heading)

        print line + "," + str(end.longitude) + "," + str(end.latitude)

        # Ready for next line
        #line = file.readline()
