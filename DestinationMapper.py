#!/usr/bin/env python

import sys

# Iterate over each line provided
for line in sys.stdin:
    line = line.strip()     # remove any leading and trailing whitespace

    if not line.startswith("OID"):  # skip header

        # Tokenize CSV into fields
        fields = line.split(",")

        if len(fields)==9:      # Ensure expected number of fields.
            # TODO: use proper CSV parser which can cope with escaped strings

            # Key = destination, Value = 1 (ready for the reduce to sum)
            dest = fields[2]
            if dest != "":      # TODO: consider how to handle empty destinations
                print fields[2] + "\t" + "1"





# Run as
# hadoop jar /usr/hdp/2.5.3.0-37/hadoop-mapreduce/hadoop-streaming.jar  -input /user/hive/dssa1/voyages.csv -output /user/hive/dssa1/out7 -mapper DestinationMapper.py -file DestinationMapper.py -reducer CountReducer.py -file CountReducer.py
