#!/usr/bin/env python

import sys
from datetime import datetime

# Convert from one date/time format to another
def convert(date):
    if date != "":
        date = datetime.strptime(date, "%Y/%m/%d %H:%M:%S")
        return date.strftime("%Y-%m-%d %H:%M:%S")
    else:
        return ""

# Iterate over each line provided
for line in sys.stdin:
    line = line.strip()     # remove any leading and trailing whitespace

    if not line.startswith("OID"):  # skip header

        # Tokenize CSV into fields
        fields = line.split(",")

        if len(fields)==9:      # Ensure expected number of fields.
            # TODO: use proper CSV parser which can cope with escaped strings

            # Generate new CSV line
            print fields[0] + "," + \
                  fields[1] + "," + \
                  fields[2] + "," + \
                  fields[3] + "," + \
                  fields[4] + "," + \
                  convert(fields[5]) + "," + \
                  convert(fields[6]) + "," + \
                  convert(fields[7]) + "," + \
                  fields[8]


# Run as
# hadoop jar /usr/hdp/2.5.3.0-37/hadoop-mapreduce/hadoop-streaming.jar -D mapred.reduce.tasks=0 -input /user/hive/dssa1/voyages.csv -output /user/hive/dssa1/out6 -mapper DateMapper.py  -file DateMapper.py


