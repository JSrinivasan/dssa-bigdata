# Modified from https://ruslanspivak.com/lsbaws-part1/

import socket
import sys

# Extract command line options
filename = sys.argv[1]
port = int(sys.argv[2])

# Set up socket
listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind(('', port))
listen_socket.listen(1)

print 'Serving HTTP on port %s ...' % port

# Send out each line of the file in turn the the requestors
with open(filename) as file:
    for line in file:
        client_connection, client_address = listen_socket.accept()
        print client_address
        request = client_connection.recv(1024)
        http_response = "HTTP/1.1 200 OK\n" + "\n" + line
        print http_response
        client_connection.sendall(http_response)
        client_connection.close()

